/** At the lower level, each component is just a javascript function that
 * returns jsx (or html)
 */
import React from 'react';

export default () => {
  return <p>I'm a person</p>;
};

