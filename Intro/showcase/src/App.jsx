import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';
/** importing the constructed component */

export default class App extends Component {
	/** React always calls this method when rendering something to the screen via ReactDOM
   * A render function always will return the html that the component will generate.
   */
	render() {
		/** At least take 3 params, receive what element you want to render (html or
     * component), the second param is a configuration object (to passing props or
     * classes), and the last element is the element children, which can hold 
     * another call to the createElement function. So this is how jsx is working, 
     * via calling createElement. This block of code: 
     * 
     * <div className="App">
     *  <h1>Hi I'm a React App</h1>
     * </div>
     * 
     * Transpiles to this under the hood. This is why it React is needed to be imported
     * a the top, along with the Component Class. Also is important to note that
     * the code is not html, it ultimately is javascript
     *  */
		// return React.createElement('div', { className: 'App' }, React.createElement('h1', null, 'Hi, I\'m a  React App'));
		return (
			<div className='App'>
				<h1>Hi I'm a React App</h1>
				<Person />
			</div>
		);
	}
}
/** JSX limitations
 * Because of class is a reserved name in javascript, to attach classes to html elements
 * className has to be used and it will be rendered as class.
 * JSX cannot have more than one element in each component (vue similitude)
*/
