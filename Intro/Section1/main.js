/** Every component is a function which returns a compiled html template
 * Later this will be done using jsx, but in the end it all comes down to
 * javascript. The react package will convert it to html. An important note
 * here: the class keyword is a reserved word in js, react solves this 
 * by using the word className
 * 
 * The function can recieve props to be dynamic. Props can be accessed with 
 * single curly braces syntax. Double was my standard
 */
function Person(props) {
  return (
  <div className="person">
    <h1>{ props.name }</h1>
    <p>Your age: { props.age }</p>
  </div>
  );
}

/** A component (like vue) also supports one root element */
function App() {
  return (
  <div>
    <Person name="Max" age="28"/>
    <Person name="Manu" age="29"/>
  </div>
  );
}

/** ReactDOM function render is used to transform javascript as a single
 * component that can be mounted in the real dom. It recieves a special
 * html item which gets parsed by jsx in the function above, and the second
 * parameter selects where it is going to be mounted
 */
// ReactDOM.render(<Person name="Max" age="28"/>, document.querySelector('#p1'));
// ReactDOM.render(<Person name="Manu" age="29"/>, document.querySelector('#p2'));

/** Is far more common to embed react components and call the ReactDOM render function
 * only once:
 */

ReactDOM.render(<App/>, document.querySelector("#app"));

/** Why choose react:
 * UI state becomes difficult to handle with vanilla
 * Focus on business logic.
 * 
 */