/** Classes can inherit from parent classes (regular oop) 
 * with this all methods are properties from the parent are inherited
 * to the child, however, to be accessed properly the "super"
 * constructor must be called in the child constructor. The super calls
 * the parent constructor to correctly initialize the parent class
 * 
 */

class Human {
	constructor() {
		this.gender = 'male';
	}
	printGender() {
		console.log(this.gender);
	}
}

class Person extends Human {
	/** called when class is instantiated */
	constructor() {
		super();
		this.name = 'Max';
		this.gender = 'female';
	}
	printName() {
		console.log(this.name);
	}
}

const person = new Person();
person.printName();
person.printGender();

/** In ES7+ constructor is no longer needed (under the hood it still is used)
 * but syntactically this way is shorter and every property holds a value 
 * or a function.
 */

class Human7 {
	gender = 'male';
	printGender = () => console.log(this.gender);
}

class Person7 extends Human7 {
	name = 'Max';
	printName = () => console.log(this.name);
}

const person7 = new Person7();
person7.printName();
person7.printGender();

/** Spread and rest operators
 * ... 
 * The syntax is the same for both operations
 * Spread is used to split up array or object properties. 
 * ex: const newArray = [...oldArray, 1, 2, 3]
 * const newObject = { ...oldObject, newProp: 5}
 * As a note in the second example if theres a property with the same key, 
 * the old value is overwritten.
 * 
 * Rest: Used to merge a list of function arguments into an array
 * fn sortArgs(...args) {
 *  return args.sort();
 * }
 */
const numbers = [ 1, 2, 3 ];
const newNumbers = [ ...numbers, 4 ];
console.log(newNumbers);

const personb = { name: 'Max' };
const newPersonb = { ...personb, age: 28 };
console.log(newPersonb);

const filterPair = (...args) => args.filter(el => el % 2 === 0);

console.log(filterPair(1, 2, 3, 4, 5, 6, 7, 8));

/** Destructuring
 * Extract array elements or objects properties and store them in variables
 * it works in arrays and objects
 * ex: For arrays (the order determines which value will take each variable)
 * [a, b] = ['Hello', 'Max']
 * For objects: (the value is determined by the key)
 * { name } = { name: 'max', age: 28 }
 */

[ a, secondNumber, , c ] = newNumbers;
console.log(secondNumber, c);

[ , , , a ] = newNumbers;
console.log(a);

/** Array methods return a new array */
const doubleNumArray = newNumbers.map(el => el * 3);
